

# Matrice de corrélation de nos données simulées et plots -----------------

plotdonnees <- function(data, type = "variable_quanti", variable=NA){

if(type=="corr"){
  plot <- corrplot::corrplot(cor(st_drop_geometry(data[,!names(data) %in% c("i","j","id")])), is.corr = T, method = "number")
  res <- plot
}else if(type=="variable_quanti"){
  library(ggplot2)
  library(ggspatial)
  
  if(is.na(variable)){print("Spécifier un nom de variable à représenter")
  }else if(variable == "respondent"){
    plot <- ggplot() +
            layer_spatial(data, aes(fill = respondent)) +
            scale_fill_fermenter(name = "respondent\n", 
                           palette = "Oranges", 
                           na.value = "white",
                           breaks = c(0,1,500,1000,1500,3000),
                           direction = 1) +
            theme_minimal()
    plot
  }else{
    name_variable <- paste0(variable, "\n")
    plot <- ggplot() +
            layer_spatial(data, aes(fill = .data[[variable]])) +
            scale_fill_fermenter(name = name_variable, 
                           palette = "Oranges", 
                           na.value = "white",
                           breaks = unname(quantile(data[[variable]])),
                           direction = 1) +
            theme_minimal()
    plot
  }}else if(type=="variable_quali"){
    plot <- ggplot() +
      layer_spatial(data, aes(fill = .data[[variable]])) +
      scale_fill_discrete() +
      theme_minimal()
    plot
  }else{print("type='corr' pour afficher la matrice de corrélation des variables; type='variable' pour afficher les valeurs d'une variable sur la carte (danc ce cas, spécifier le nom de la variable)")}
  }
