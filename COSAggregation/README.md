
# COSAggregation

<!-- badges: start -->
<!-- badges: end -->

Ce package permet de réaliser une classification avec contraintes de contiguïté et de nombre minimum de répondants.

## Installation

Pour installer le package, télécharger le code source :

https://gitlab.com/rovatina/methodologie-clustering-contiguite/-/raw/main/COSAggregation_0.0.0.9000.tar.gz

Ou bien :

https://gitlab.com/rovatina/methodologie-clustering-contiguite/-/blob/main/COSAggregation_0.0.0.9000.tar.gz?ref_type=heads

Ensuite, installer le package :

``` r
install.packages("~chemin/vers/le/package/COSAggregation_0.0.0.9000.tar.gz", repos = NULL, type = "source")
```
