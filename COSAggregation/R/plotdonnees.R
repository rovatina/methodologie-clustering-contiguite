#' Matrice de corrélation des données simulées et plots
#'
#' @param data base de données avec géographie
#' @param type type de variable à représenter
#' @param variable nom de la variable
#'
#' @return representations graphiques
#' @export
#'
#' @examples
#' plotdonnees(grille900$data, type = "variable_quanti", variable="economic")
#' plotdonnees(grille900$data, type = "variable_quanti", variable="population")
#' plotdonnees(grille900$data, type = "variable_quanti", variable="Repondants")
#' plotdonnees(grille900$data, type = "variable_quali", variable="qualitative")

#' @importFrom corrplot corrplot
#' @importFrom ggplot2 ggplot scale_fill_fermenter theme_minimal scale_fill_discrete
#' @importFrom ggspatial layer_spatial aes
#' @importFrom sf st_drop_geometry
#' @importFrom stats cor quantile
plotdonnees <- function(data, type = "variable_quanti", variable=NA){

  if(type=="corr"){
    plot <- corrplot::corrplot(stats::cor(sf::st_drop_geometry(data[,!names(data) %in% c("i","j","Nom","qualitative", "Nombre_villes")])), is.corr = T, method = "number")
    res <- plot
  }else if(type=="variable_quanti"){
    if(is.na(variable)){print("Spécifier un nom de variable à représenter")
    }else if(variable == "Repondants"){
      plot <- ggplot2::ggplot() +
        ggspatial::layer_spatial(data, ggspatial::aes(fill = Repondants)) +
        ggplot2::scale_fill_fermenter(name = "Répondants\n",
                             palette = "Oranges",
                             na.value = "white",
                             breaks = c(0,1,500,1000,1500,3000),
                             direction = 1) +
        ggplot2::theme_minimal()
      plot
    }else{
      name_variable <- paste0(variable, "\n")
      plot <- ggplot2::ggplot() +
        ggspatial::layer_spatial(data, ggspatial::aes(fill = .data[[variable]])) +
        ggplot2::scale_fill_fermenter(name = name_variable,
                             palette = "Oranges",
                             na.value = "white",
                             breaks = unname(stats::quantile(data[[variable]])),
                             direction = 1) +
        ggplot2::theme_minimal()
      plot
    }}else if(type=="variable_quali"){
      plot <- ggplot2::ggplot() +
        ggspatial::layer_spatial(data, ggspatial::aes(fill = .data[[variable]])) +
        ggplot2::scale_fill_discrete() +
        ggplot2::theme_minimal()
      plot
    }else{print("type='corr' pour afficher la matrice de corrélation des variables; type='variable' pour afficher les valeurs d'une variable sur la carte (danc ce cas, spécifier le nom de la variable)")}
}
