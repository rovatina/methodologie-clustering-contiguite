#' Créer une matrice de contiguité à partir d'une grille
#'
#' @param df
#'
#' @return un dataframe
create_contiguity_matrix <- function(df){
  size <- sqrt(nrow(df))
  matrix <- matrix(0, size^2, size^2)
  for (i in 1:size){
    for (j in 1:size){
      id = size*(i - 1) + j

      # contigu au voisin de gauche
      if (j != 1){
        matrix[id - 1, id] <- 1
      }

      # contigu au voisin de droite
      if (j != size){
        matrix[id, id + 1] <- 1
      }

      # contigu au voisin d'en haut
      if (i != 1){
        matrix[id - size, id] <- 1
      }

      # contigu au voisin d'en bas
      if (i != size){
        matrix[id, id + size] <- 1
      }
    }
  }
  return(matrix + t(matrix)) # Ajout de la transposée vu qu'on a calculé uniquement sur le triangle supérieur (la matrice est symmétrique)
}
